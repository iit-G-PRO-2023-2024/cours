# Cours

pulication qrcode


npm install -g eas-cli


eas login


```
eas update --branch [branch-name] --message "..."
# Example
eas update --branch version-1.0 --message "Fixes typo"
```




lien gitlab : http://tiny.cc/cours-g-pro

lien sheet: https://docs.google.com/spreadsheets/d/1Pg_uxdh4WGrVBUH5yIBc3eMLDYkObFgR4ZshfELFsco/edit#gid=0 


storybook : https://maurogarcia.dev/posts/Setup-storybook-with-expo/


# integrate storybook

npx -p @storybook/cli sb init --type react_native


# flex box 

https://mescours.ovh/ex10.html

https://snack.expo.dev/@amina-abdelkafi/flex-example


# Install Expo Router

https://docs.expo.dev/routing/installation/

# Platform-specific Modules

https://docs.expo.dev/router/advanced/platform-specific-modules/


# React Native Path Aliasing with @


https://javascript.plainenglish.io/react-native-path-aliasing-with-simplify-your-imports-and-enhance-your-codebase-9897efee96a8





## Redux

https://reintech.io/blog/how-to-use-react-native-with-redux



## TOKEN
eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhNjg5ZGQyZC0yNWJiLTRlMGItYmQ5OS0zNGEyZmFmYzNiY2QiLCJleHAiOjE3MDIwNjI3MjIsImlhdCI6MTcwMDc2NjcyMiwidHlwZSI6ImFjY2VzcyIsInJvbGVzIjpbInVzZXIiXX0.vpUc-v5DVxGzA-F6FYiejod5rObLy3-MoEG85_PZ2b0


# url swagger 
https://iit.aminaabdelkafi93.workers.dev/swagger

# url doc 
https://iit.aminaabdelkafi93.workers.dev/doc


# generate service 

https://www.npmjs.com/package/swagger-typescript-api

# demo compte

{
  "password": "admin",
  "email": "admin@gmail.com"
}

# formulaire d'evaluation 

https://forms.gle/Qz88yJGsLWENTtcG8


# objectif d'examen

L'objectif de cette tâche est d'ajouter un bouton "Promo" dans le menu de l'application, situé à côté du bouton de notifications. Ce bouton "Promo" doit être fonctionnel et rediriger l'utilisateur vers la liste des promotions lorsqu'il est cliqué.

https://www.figma.com/file/jGi4Z7GXIAgENJV81JZCt1/iit-project?type=design&node-id=9141-6511&mode=design&t=XVunJY6IWlLf0k9l-0

### Tâches à Effectuer :

1. Création du Bouton :
  
   - Ajouter un bouton intitulé "Promo" dans le menu de l'application.
  
   - Positionner le bouton à côté du bouton de notifications.
  
   - Utiliser le design existant disponible sur Figma pour assurer la cohérence visuelle.

2. Configuration du Bouton :
  
   - Définir une action pour le bouton "Promo" de manière à ce qu'il soit cliquable.
  
   - Configurer l'action pour rediriger l'utilisateur vers la liste des promotions.

3. Création du Composant "Card Produit" :
  
   - Concevoir le composant "Card Produit" avec les éléments visuels nécessaires pour représenter les détails d'un produit.
   

4. Développement du Composant en Storybook :
  
   - Créer le composant "Card Produit" dans Storybook.
  
   - Intégrer le composant avec des exemples de données simulées pour illustrer son fonctionnement dans différentes situations.
  
   - Assurer la cohérence avec le design global de l'application.

5. Affichage de la Liste des Produits de promotion:
   
   - Mettre en place une page dédiée à l'affichage de la liste des produits de promotion.
   
   - Utiliser le composant "Card Produit" pour afficher la liste des produits de promotion
