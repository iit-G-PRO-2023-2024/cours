import { Button, Image, StyleSheet, Text, View } from 'react-native'
import React, { useState } from 'react'

const FirstComponent = () => {
 const [ count , setCount ] = useState(0)


 const increment = () => {

  
 }

  return (
    <View style={{height : 100 , width : 100}}>
      <Text>FirstComponent.  count:  {count} </Text>
      <Button title='submit' onPress={()=>{setCount(count+1)}}/>

      <Image style={{height : "100px" , width:"100px"}} source={{uri: 'https://picsum.photos/100/100' }}></Image>
    </View>
  )
}

export default FirstComponent

const styles = StyleSheet.create({})